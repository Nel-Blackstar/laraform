<?php


function uniqueCode(){
    	$val=strtoupper(str_random(5));
    	if (DB::table("membres")->whereCode($val)->count()>0) {
    		uniqueCode();
    	}else{
    		return $val;
    	}
    	
    }

function construire($para){
	$membre=DB::table("membres")->whereCode($para)->first();
	$fg=DB::table("membres")->whereCode($membre->fg)->first();
	$fd=DB::table("membres")->whereCode($membre->fd)->first();
		?>
		<ul>
			<?php
						if (!empty($fg)) {
			?>
			<li>
				<a href="#" onclick="voir('<?=$fg->code; ?>')">
					<img src="images/user.jpg" alt="" width="70px" height="65px"><br>
                     <table>  
                   <tr><td class="pull-right"> Code : </td><td><span class="pull-left text-primary"><?=$fg->code; ?></span></td></tr>
                    <tr><td class="pull-right"> Prenom : </td><td><span class="pull-left text-primary"><?=$fg->prenom; ?></span></td></tr>
                    <tr><td class="pull-right"> Nom :</td><td> <span class="pull-left text-primary"><?=$fg->nom; ?></span></td></tr>
                    </table>
				</a>
				<?php  construire($fg->code);?>
			</li>
			<?php 
								}
			?>
			<?php
						if (!empty($fd)) {
			?>
			<li>
				<a href="#" onclick="voir('<?=$fd->code; ?>')">
					<img src="images/user.jpg" alt="" width="70px" height="65px"><br>
                     <table>  
                   <tr><td class="pull-right"> Code : </td><td><span class="pull-left text-primary"><?=$fd->code; ?></span></td></tr>
                    <tr><td class="pull-right"> Prenom : </td><td><span class="pull-left text-primary"><?=$fd->prenom; ?></span></td></tr>
                    <tr><td class="pull-right"> Nom :</td><td> <span class="pull-left text-primary"><?=$fd->nom; ?></span></td></tr>
                    </table>
				</a>
				<?php  construire($fd->code);?>
			</li>
							<?php 
								}
							?>
		</ul>
		<?php
	}

	
function positionLibre($bsuser,$coter){
		$position=DB::table("membres")->whereCode($bsuser)->first();
		$position=$position->$coter;
		$bio=positionLibreDef($position);
		return $bio;
	}
function positionLibreDef($bsuser){
		$pere=DB::table("membres")->whereCode($bsuser)->first();
		if (!empty($pere->fg) && !empty($pere->fd)) {
			if ($pere->fg) {
				$posi="fd";
			}elseif ($pere->fd) {
				$posi="fg";
			}
		return positionLibre($pere->code,$posi);
		}else{
		if (empty($pere->fg)) {
			$info["position"]=$bsuser;
			$info["coter"]="fg";
		}elseif (empty($pere->fd)) {
			$info["position"]=$bsuser;
			$info["coter"]="fd";
		}
		return $info;
	}
}