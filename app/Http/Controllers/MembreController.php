<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\InsertionRequest;
use App\membre;

class MembreController extends Controller
{
    public function show(){
    	$user=membre::first();
    	$users=membre::all();
    	return view("home",["user" => $user,"users" => $users]);
    }

    public function store(Request $request){
    	$code=uniqueCode();
    	membre::create(["code" => $code,"nom" => $request->nom,"prenom" => $request->prenom,"naissance" => $request->naissance,]);
    	return redirect("/");
    }
    public function update(Request $request2){
    	$code=positionLibre($request2->bsuser,$request2->coter);
    	$membrev = membre::where('code', '=', $code['position'])->update(array($code['coter'] => $request2->codeuser));
    	$membreupdate = membre::where('code', '=', $request2->codeuser)->update(array("code_parrain" => $code['position']));
    	return redirect("/");
    }

}
