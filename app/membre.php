<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class membre extends Model
{
    protected $fillable = [ 'code', 'nom','prenom',"coter","fg","fd","naissance"];

    public $timestamps=false;
}
