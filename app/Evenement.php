<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evenement extends Model
{
		protected $fillable = ['nom','titre','description','prix',];

		public $timestamps=false;

		protected $casts = [
			'prix' => 'float'
		];
}