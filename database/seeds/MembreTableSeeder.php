<?php

use Illuminate\Database\Seeder;
use  App\membre;

class MembreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        membre::create([
            'code' => 'BSN237',
            'nom' => 'Blackstar',
            'prenom' => 'Nelson',
            'naissance' => '20/08/1998',
            'fg' => 'BSN238',
            'fd' => 'BSN239',
        ]);
        membre::create([
            'code' => 'BSN238',
            'nom' => 'Mogo',
            'prenom' => 'Ivan',
            'naissance' => '20/08/1999',
            'code_parrain' => 'BSN237',
        ]);
        membre::create([
            'code' => 'BSN239',
            'nom' => 'Kamgain',
            'prenom' => 'Borel',
            'naissance' => '20/08/2000',
            'code_parrain' => 'BSN237',
        ]);
}
}
