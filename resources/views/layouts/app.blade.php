<html>
<head>
	<title>{{ config('app.name') }} - @yield("titre")</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width:device-width;initial-scale:1">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	@yield('css')
</head>
<body>
	@yield('contenue')
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>