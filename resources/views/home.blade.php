@extends('layouts.app')

@section("titre")
Home
@endsection

@section("contenue")
<nav class="navbar navbar-dark bg-dark text-dark">
      <a class="navbar-brand" href="#">MLM BLACK </a>
      <ul class="nav navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#">Acceuil <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Matrice</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Liste de Utilisateurs</a>
        </li>
      </ul>
      <form class="form-inline navbar-form pull-right">
        <input required class="form-control " type="text" placeholder="Search">
        <button class="btn btn-primary " type="submit">Rechercher</button>
      </form>
    </nav>
    <div class="container-fluid">
      <h2 align="center" class="text-uppercase text-success">acceuil</h2>
      <hr>
      <!--  affichage des users-->
      <div align="center" class="col-sm-4 text-uppercase"  style="border-left: 4px solid;height: 60%;max-height: 60%;overflow: auto;">
        <h6 class="text-primary">Liste des users</h6>
        <table class="table table-responsive ">
          <thead>
            <tr class="bg-danger"><th>Code</th><th>Nom</th><th>Prenom</th><th>Naissance</th><th>Parrent</th></tr>
          </thead>
          <tbody>
        @foreach($users as $membre)
         <tr>
          <td>{{$membre->code}}</td><td>{{$membre->nom}}</td><td>{{$membre->prenom}}</td><td>{{$membre->naissance}}</td>
          <td>{{$membre->code_parrain}}</td>
         </tr>
        @endforeach
        </tbody>
        </table>
      </div>
      <!-- Debut de l'insertion-->
       <div align="center" class="col-sm-4 text-uppercase"  style="border-left: 4px solid;height: 60%;">
        <h3 class="text-info">Insertion</h3><br><br>
        <form method="POST" action="/insertion">
          {{ csrf_field() }} 
            <div class="form-group">
              <label for="nom" class="form-control-label pull-left text-primary"><b>Nom</b></label>
              <input required type="text" class="form-control" id="nom" name="nom" placeholder="Votre Nom">
            </div>
            <div class="form-group">
              <label for="prenom" class="form-control-label pull-left text-primary"><b>Prenom</b></label>
              <input required type="text" class="form-control" id="prenom" name="prenom" placeholder="Votre Prenom">
            </div>
            <div class="form-group">
              <label for="naissance" class="form-control-label pull-left text-primary"><b>Date de Naisssance</b></label>
              <input required type="Date" class="form-control" id="naissance" name="naissance" placeholder="Date de Naisssance">
            </div><br><br>
            <button class="btn btn-success" type="submit">Enregistrez</button>&nbsp &nbsp
            <button class="btn btn-danger" type="reset">Annuler</button>
          </form>
      </div>
       <!--  debut de l'ajout a la matrice-->
      <div align="center" class="col-sm-4 text-uppercase"  style="border-left: 4px solid;height: 60%;">
        <h3 class="text-danger">Positionement</h3><br><br>
        <form method="POST" action="/positionement">
          {{ csrf_field() }} 
          <div class="form-group ">
              <label for="parrain" class="form-control-label pull-left text-primary"><b>Code</b></label>
              <input required type="text" class="form-control" id="parrain" name="codeuser" placeholder="Code De l'utilisateur a ajouter">
            </div>  
             <div class="form-group">
              <input type="hidden" name="bsuser" value="{{ $user->code }}">
              <label for="coter" class="form-control-label pull-left text-primary"><b>Choisir un coter</b></label>
             <select name="coter" id="coter" class="form-control">
               <option value="fg">Gauche</option>
               <option value="fd">Droit</option>
             </select>
            </div><br><br>
            <button class="btn btn-success" type="submit">Enregistrez</button>&nbsp &nbsp
            <button class="btn btn-danger" type="reset">Annuler</button>
          </form>
      </div>
      <!-- Affichage -->
      <div class="col-sm-12 border-secondary text-uppercase affichage">
        <hr>
        <h3 align="center" class="text-warning">affichage</h3>
        <hr>
        <div class="tree" style="width: 100%;justify-content: center;">
        <ul  align="center" style="width: 100%;justify-content: center;">
              <li>
                  <a href="#" onclick="voir('{{ $user->code }}')">
                     <img src="images/user.jpg" alt="" width="70px" height="65px"><br>
                     <table>  
                   <tr><td class="pull-right"> Code : </td><td><span class="pull-left text-primary">{{ $user->code}}</span></td></tr>
                    <tr><td class="pull-right"> Prenom : </td><td><span class="pull-left text-primary">{{ $user->prenom}}</span></td></tr>
                    <tr><td class="pull-right"> Nom :</td><td> <span class="pull-left text-primary">{{ $user->nom}}</span></td></tr>
                    </table>
                  </a>
                     {!! construire($user->code) !!}
                     
              </li>
         </ul>
        </div>
      </div>
    </div>
        <script>
    function voir(s)
    {
      var a=document.getElementById(s);
      if(a.style.display=='none'){
        a.style.display='block';
      }else{
        a.style.display='none';
      }
    }
  </script> 
@endsection